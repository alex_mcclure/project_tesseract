﻿
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace ProjectTesseract
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void getTextButton_Click(object sender, EventArgs e)
        {
            ScanImage scanImage = new ScanImage();
            //string imageFilePath = @"C:\Users\Draghon\Desktop\ImageProcessing\Forms_to_convert\SUPERVISORS_INVESTIGATION\SUPERVISORS_INVESTIGATION_page_002_Copy.jpg";
            string imageFilePath = @"C:\Users\alexm\Desktop\CoolTech 15 forms\SUPERVISORS_INVESTIGATION_Copy.pdf";
            //scanImage.ConvertImageToText(@"C:\Users\Draghon\Desktop\ImageProcessing\Forms_to_convert\SUPERVISORS_INVESTIGATION\SUPERVISORS_INVESTIGATION_page_002.jpg");
            scanImage.ConvertImageToText(imageFilePath);

            Thread imageThread = new Thread(() => scanImage.ConvertImageToText(imageFilePath));

        }

        private void selectFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = @"C:\";
            dlg.RestoreDirectory = true;
            dlg.Title = "Select Image or PDF Files";


            dlg.Filter = "Images (*.BMP;*.JPG;*.GIF,*.PNG,*.TIFF,*.PDF)|*.BMP;*.JPG;*.GIF;*.PNG;*.TIFF;*.PDF|All files (*.*)|*.*";
            dlg.FilterIndex = 3;

            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;

            dlg.Multiselect = true;

            //dlg.ShowDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                ScanImage scanImage = new ScanImage();
                List<string> fileList = dlg.FileNames.ToList();

                fileList.RemoveAll(a => String.IsNullOrWhiteSpace(a));

                List<Thread> fileThreadList = new List<Thread>();

                for (int i = 0; i < fileList.Count; i++)
                {
                    string fileName = fileList[i];
                    if (File.Exists(fileName))
                    {
                        fileThreadList.Add(new Thread(() => scanImage.ConvertImageToText(fileName)));

                        //scanImage.ConvertImageToText(fileName);
                    }
                }

                for (int i = 0; i < fileThreadList.Count; i++)
                {
                    fileThreadList[i].Start();
                }

                

            }
        }

        private void textBoxEnglish_TextChanged(object sender, EventArgs e)
        {


        }

        private void textBoxOther_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
