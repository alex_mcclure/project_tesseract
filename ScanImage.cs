﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge;
using AForge.Imaging;
using AForge.Math.Geometry;
using Patagames.Ocr;
using Patagames.Ocr.Enums;
using Point = System.Drawing.Point;
using ImageMagick;
using System.IO;

namespace ProjectTesseract
{
    class ScanImage
    {


        public void ConvertImageToText(string filePath)
        {
            using (var api = OcrApi.Create())
            {
                api.Init(Languages.English);

                List<Bitmap> imageList = new List<Bitmap>();
                if (filePath.EndsWith("pdf"))
                {
                    MagickImageCollection images = new MagickImageCollection();
                    MagickReadSettings settings = new MagickReadSettings();
                    settings.Density = new Density(300, 300);
                    images.Read(filePath, settings);

                    foreach (IMagickImage image in images)
                    {
                        imageList.Add(image.ToBitmap());
                    }

                }
                else
                {
                    imageList.Add(new Bitmap(filePath));
                    //formBitmap = new Bitmap(filePath);
                }


                for (int i = 0; i < imageList.Count; i++)
                {
                    proccessImage(imageList[i], api);
                }


               
                api.Release();
            }
        }

        private void proccessImage(Bitmap formBitmap, OcrApi api)
        {
            //Rectangle rectangle = new Rectangle(160, 420, 565, 240);

            //string plainText = api.GetTextFromImage(formBitmap, rectangle);
            //string plainText = api.GetTextFromImage("C:\\Users\\Draghon\\Desktop\\ImageProcessing\\Forms_to_convert\\SUPERVISORS_INVESTIGATION\\SUPERVISORS_INVESTIGATION_page_002.jpg", rectangle);

            //textBox1.Text += plainText;
            //textBox1.Text += "NEW LINE \r";

            //Lower numbers will convert more pixels to white
            List<int> greyScaleList = new List<int>()
                { 220 };
            //{ 175 };
            //{ 175, 200, 220 };


            List<Rectangle> rectangleList = new List<Rectangle>();
            List<OcrPixa> ocrPixaList = new List<OcrPixa>();

            OcrBoxa ocrBoxa;
            int[] ghghgh;

            for (int i = 0; i < greyScaleList.Count; i++)
            {
                Bitmap newBitmap = greyScaleImage(formBitmap, greyScaleList[i]);

                string saveFilePath = @"C:\PDF\" + "New_" + greyScaleList[i] + "_" + DateTime.Now.ToString("MM_dd_hh_mm_ss_ffffff") + ".png";


                if (false)
                {
                    OcrPixa tempocrPixa;
                    OcrPix orcPix = OcrPix.FromBitmap(formBitmap);
                    api.SetImage(orcPix);
                    api.GetTextlines(out ocrBoxa, out tempocrPixa, out ghghgh);
                    ocrPixaList.Add(tempocrPixa);

                    //List<Rectangle> rectangleList = new List<Rectangle>();

                    rectangleList = tempocrPixa.Boxes.ToList();

                    using (Graphics gr = Graphics.FromImage(newBitmap))
                    {
                        gr.SmoothingMode = SmoothingMode.AntiAlias;

                        //Rectangle rect = new Rectangle(10, 10, 260, 90);
                        //gr.FillEllipse(Brushes.LightGreen, rect);
                        using (Pen thick_pen = new Pen(Color.Red, 1))
                        {
                            gr.DrawRectangles(thick_pen, rectangleList.ToArray());
                        }
                    }
                }

                if (true)
                {
                    BlobCounter blobCounter = new BlobCounter();
                    blobCounter.FilterBlobs = true;
                    blobCounter.MinHeight = 40;
                    blobCounter.MinWidth = 40;

                    int maxWidth = newBitmap.Width - ((newBitmap.Width / 100) * 10);
                    int maxHeight = newBitmap.Height - ((newBitmap.Height / 100) * 10);

                    blobCounter.MaxWidth = maxWidth;
                    blobCounter.MaxHeight = maxHeight;
                    //blobCounter.BackgroundThreshold = 0;

                    blobCounter.ProcessImage(newBitmap);

                    List<Blob> blobList = blobCounter.GetObjectsInformation().ToList();

                    SimpleShapeChecker shapeChecker = new SimpleShapeChecker();

                    Random rnd = new Random();
                    for (int h = 0; h < blobList.Count; h++)
                    {
                        Blob blob = blobList[h];

                        List<IntPoint> edgePoints = blobCounter.GetBlobsEdgePoints(blob);
                        List<IntPoint> cornerPoints;

                        if (shapeChecker.IsQuadrilateral(edgePoints, out cornerPoints))
                        {
                            rectangleList.Add(blob.Rectangle);
                            //Needs to be off or need to implement finding other shapes and turning into rectangles
                            //if (shapeChecker.CheckPolygonSubType(cornerPoints) == PolygonSubType.Rectangle)
                            {
                                List<Point> pointList = new List<Point>();
                                foreach (var item in cornerPoints)
                                {
                                    pointList.Add(new Point(item.X, item.Y));
                                }

                                Graphics g = Graphics.FromImage(newBitmap);

                                Color color = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                                g.DrawPolygon(new Pen(color, 5.0f), pointList.ToArray());

                            }
                        }

                    }


                    //List<Rectangle> rectangleList = blobCounter.GetObjectsRectangles().ToList();
                    //if (rectangleList != null && rectangleList.Count > 0)
                    //{
                    //    using (Graphics gr = Graphics.FromImage(newBitmap))
                    //    {
                    //        gr.SmoothingMode = SmoothingMode.AntiAlias;

                    //        //Rectangle rect = new Rectangle(10, 10, 260, 90);
                    //        //gr.FillEllipse(Brushes.LightGreen, rect);

                    //        for (int h = 0; h < rectangleList.Count; h++)
                    //        {

                    //            using (Pen thick_pen = new Pen(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)), 3))
                    //            {
                    //                gr.DrawRectangles(thick_pen, new Rectangle[] { rectangleList[h] });
                    //            } 
                    //        }
                    //    }
                    //}

                }


                SaveBitmap(newBitmap, greyScaleList[i].ToString());

                //newBitmap.Save(saveFilePath, System.Drawing.Imaging.ImageFormat.Png);
            }


            if (true)
            {
                //rectangle = new Rectangle(0, 0, formBitmap.Width, formBitmap.Height);
                //string plainText = api.GetTextFromImage(formBitmap);






                //textBox1.Text += plainText;



                //Dictionary<string>
                Bitmap textBitmap = greyScaleImage(formBitmap, 170);

                // rectangleList = ocrPixa1.Boxes.ToList();

                List<string> textList = new List<string>();

                for (int i = 0; i < rectangleList.Count; i++)
                {
                    textList.Add(api.GetTextFromImage(textBitmap, rectangleList[i]));
                }

                bool temp = true;
            }
        }

        private Bitmap greyScaleImage(Bitmap origbitmap, int colourLine = 170)
        {
            Color c;
            Bitmap bitmap = (Bitmap)origbitmap.Clone();

            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    c = bitmap.GetPixel(i, j);
                    Color newColor = c;
                    byte gray = (byte)(.299 * c.R + .587 * c.G + .114 * c.B);

                    double hue = c.GetHue();
                    double saturation = c.GetSaturation();
                    //double lightness = c.GetBrightness();
                    double value = 0;

                    ColorToHSV(newColor, out hue, out saturation, out value);

                    if (c.R < colourLine && c.G < colourLine && c.B < colourLine)// && value < 0.60)
                    {
                        //if (c.R < 100 && c.G < 100 && c.B < 100)
                        {
                            newColor = Color.Black;
                        }
                    }
                    else
                    {
                        newColor = Color.White;
                    }

                    bitmap.SetPixel(i, j, newColor);
                }
            }            
            return bitmap;
        }

        public void ColorToHSV(Color color, out double hue, out double saturation, out double value)
        {
            int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));

            hue = color.GetHue();
            saturation = (max == 0) ? 0 : 1d - (1d * min / max);
            value = max / 255d;
        }


        private bool SaveBitmap(Bitmap bitmap, string fileName)
        {
            bool success = false;

            string saveFilePath = @"C:\PDF\" + "New_" + fileName + "_" + DateTime.Now.ToString("MM_dd_hh_mm_ss_ffffff") + ".png";

            bitmap.Save(saveFilePath, System.Drawing.Imaging.ImageFormat.Png);

            if (File.Exists(saveFilePath))
            {
                success = true;
            }

            return success;
        }

    }
}
